# Mike Jang's Qualifications

Mike Jang's qualifications for the Circle Technical Writer position.

- Resume: mjangResume_circle.docx
- Writing Samples: listOfWritingSamples.md
- Cover Letter: coverLetter_circle.docx
- Speaking Experience: speakingExperience.md

