I’m including links to writing samples that represent my latest and best work, in several categories. I've described my role in each linked document, along with the following details. 

* Toolchain
* SME
* Author

A) Documentation from Scratch: Getting Started Guide

I created the first product documentation for Cobalt.io, including a [Getting Started Guide](https://developer.cobalt.io).

| Criteria      | Description             |
| ------------- |-------------------------|
| Toolchain     | Hugo / Docsy            |
| SME           | Multiple PMs/Developers |
| Author        | Myself                  |

I also set up the associated [GitHub repository](https://github.com/cobalthq/cobalt-product-public-docs) from scratch, while learning a new documentation toolchain.

B) API Reference 

I set up documentation for a “Postman” collection: [ForgeRock Cloud APIs](https://documenter.getpostman.com/view/5389909/SVfRu82V?version=latest). 

Additional Information:

| Criteria      | Description                      |
| ------------- |----------------------------------|
| Toolchain     | Postman Pro (Markdown)           |
| SME           | UI Developers, Program Managers  |
| Author        | Myself                           |

I demonstrated the use of this collection at the end of a sprint.

<br>

C) Procedure: Configuring User Self-Service

I wrote the following chapter to help developers create their own on user self-service UIs with REST commands and JSON code samples: [Configuring User Self-Service](https://backstage.forgerock.com/docs/idm/6.5/integrators-guide/#chap-ui-uss).

Additional Information:

| Criteria      | Description                                           |
| ------------- |-------------------------------------------------------|
| Toolchain     | DocBook / Java                                        |
| SME           | UI Developers, Engineering Director, Program Manager  |
| Author        | Myself                                                |


<br>

D) Conceptual Guide: Getting Started With IDM

I created the following mostly conceptual guide, to introduce newer users to a ForgeRock product: [Getting Started With IDM](https://backstage.forgerock.com/docs/idm/6.5/getting-started/).

Additional Information:

| Criteria      | Description         |
| ------------- |---------------------|
| Toolchain     | DocBook / Java      |
| SME           | Multiple developers |
| Author        | Myself              |

I used a scenario suggested by my manager and the product engineering director. As this was an original guide, I asked all (9) other writers within our group to review the guide. 

<br>

E) Procedure: Securing a System

Based on my knowledge of Linux, I created the following appendix to help users secure their installations: [Installing on a Read-Only Volume](https://backstage.forgerock.com/docs/idm/6.5/install-guide/index.html#appendix-ro-install).

Additional Information:

| Criteria      | Description   |
| ------------- |---------------|
| Toolchain     | DocBook / Java|
| SME           | Myself *      |
| Author        | Myself        |

* Given my Linux experience, the relevant engineering manager accepted me as an author and SME on this document. However, I also got reviews from a relevant developer and QA engineer.

<br>

F) Blog post

I created this blog post, as it was the simplest way to document integration between three products: [ForgeRock Identity Platform Version 6: Integrating IDM, AM, and DS](https://web.archive.org/web/20221202130440/https://forum.forgerock.com/2018/05/forgerock-identity-platform-version-6-integrating-idm-ds/). 

Additional Information:

| Criteria      | Description                            |
| ------------- |----------------------------------------|
| Toolchain     | WordPress                              |
| SME           | Multiple Developers, Document Manager  |
| Author        | Myself                                 |


I used the content of the blog post to demonstrate integration between these products at an offsite unconference with customers and partners.

G) Educational Content

I've written a number of technical books, mostly on Linux. I include a link to a book that's designed to help readers pass the "hands-on" Red Hat Certified Engineer Exam.

While the content is copyrighted, it's available from booksellers such as Amazon:

- [RHCSA/RHCE Red Hat Linux Certification Study Guide, Seventh Edition](https://www.amazon.com/RHCSA-Linux-Certification-Study-Seventh-ebook/dp/B01DB3H8AM)

Additional Information:

| Criteria      | Description                            |
| ------------- |----------------------------------------|
| Toolchain     | LibreOffice                            |
| SME           | Myself, Technical Editors              |
| Author        | Myself                                 |

This is one of 20+ books that I've written. Several colleges have used my books in their courses.
